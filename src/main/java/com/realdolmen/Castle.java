package com.realdolmen;

import com.realdolmen.domain.Soldier;
import com.realdolmen.services.SoldierCommands;
import com.realdolmen.services.SoldierServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Castle {

    private static Logger logger = LoggerFactory.getLogger(Castle.class);
    private static final SoldierCommands soldierService = new SoldierServiceImpl();
    private static final Scanner scanner = new Scanner(System.in);
    private static List<Soldier> mySoldiersList = new ArrayList<>();

    public static void main(String[] args) {

        logger.info("Welcome to my Kingdom!");
        logger.debug("Debug logger");
        logger.error("Error logger");
        logger.trace("Trace logger");
        logger.warn("Warn logger");


        System.out.println("Who is your king?");
        System.out.println("Our king is: " + Soldier.MY_KING); //Voorbeeldje van hoe een static werkt

        //je kan de volgende 4 lijnen code in een while(true) plaatsen als je wilt dat uw programma blijft lopen
        System.out.println("-------");
        System.out.println("What does the king have to do?");
        showAvailableOptions();
        chooseOption();
    }

    private static void chooseOption() {
//        je kan om de switch korter te schrijven, gebruik maken van de command pattern
//        https://www.tutorialspoint.com/design_pattern/command_pattern.htm
        switch (scanner.nextInt()) {
            case 1:
                //Geweldig nu ik streams mag gebruiken :p
                //Split is een methode die je kan gebruiken om bijv strings te splitten op basis van hun separator (bijv spaces of komma's)
                //Split returns een Array van Strings
                Arrays.stream(scanner.nextLine().split(" ")).forEach(soldiersName -> mySoldiersList.add(createSoldier(soldiersName)));
                break;
            case 2:
                //loop over mijn lijst met soldaten en stuur elke soldaat naar de barakken
                mySoldiersList.forEach(soldier -> soldierService.sendToBarracks(soldier));
                break;
            case 3:
                showAllSoldiersInBarrack();
                break;
        }
    }

    private static void showAvailableOptions() {
        System.out.println("\t1. Create a soldier");
        System.out.println("\t2. Send soldier to the barracks");
        System.out.println("\t3. Show a list of all soldiers in the barrack");
    }

    //Dit is een vereenvoudigde vorm van de Factory Pattern (concreet een methode die de verantwoordelijkheid heeft om objecten te maken)
    //https://www.tutorialspoint.com/design_pattern/factory_pattern.htm
    private static Soldier createSoldier(String name) {
        return Soldier.builder(name).build();
    }

    private static void showAllSoldiersInBarrack() {
        List<Soldier> soldiers = soldierService.findAll();
        System.out.println("All soldiers in barrack");
        soldiers.forEach(System.out::println);
    }
}
