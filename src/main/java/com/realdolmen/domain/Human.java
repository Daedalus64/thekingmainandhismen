package com.realdolmen.domain;

import com.sun.istack.internal.NotNull;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Objects;

@Getter
@Setter
@SuperBuilder(builderMethodName = "humanBuilder")
public class Human {
    private int legs;
    private int arms;
    private String name;
    private int head;
    private int id;

    public static HumanBuilder builder(){
        return humanBuilder().legs(2).head(1).arms(2);
    }


}
