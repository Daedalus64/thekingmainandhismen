package com.realdolmen.services;

import com.realdolmen.domain.Soldier;
import com.realdolmen.repository.BarackRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class SoldierServiceTest {

    @InjectMocks
    private SoldierCommands soldierService = new SoldierServiceImpl(); //SUT

    @Mock
    private BarackRepository barackRepository; //MOCK

    @Test
    @DisplayName("Be sure that sent to barrack calls repo one time")
    public void assertSentToBarracks() {
        Soldier soldier = Soldier.builder("Max").build();
        soldierService.sendToBarracks(soldier);
        Mockito.verify(barackRepository).save(soldier);
    }

    @Test
    public void assertThatSoldierListIsNotEmpty() {
        Mockito.when(barackRepository.findAllFromDb()).thenReturn(Arrays.asList(Soldier.builder("Rex").build(), Soldier.builder("Jack").build()));
        List<Soldier> soldiers = soldierService.findAll();
        Mockito.verify(barackRepository).findAllFromDb();
        Assertions.assertFalse(soldiers.isEmpty());
        Assertions.assertEquals("Rex", soldiers.get(0).getName());
    }

}
